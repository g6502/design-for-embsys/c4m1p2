library ieee;
use ieee.std_logic_1164.all;

entity C4M1P2 is port
	(
		SW		: IN STD_LOGIC_VECTOR (9 downto 0);
		HEX0 	: OUT STD_LOGIC_VECTOR (7 downto 0);
		HEX1 	: OUT STD_LOGIC_VECTOR (7 downto 0)
	);
end entity C4M1P2;

architecture implementation of C4M1P2 is 

signal V 	: std_logic_vector(3 downto 0) := SW(3 downto 0);
signal A 	: std_logic_vector(3 downto 0);	--
signal z 	: std_logic;	--
signal d0	: std_logic_vector(3 downto 0);	
signal d1	: std_logic_vector(3 downto 0);

--component C4M1P1 is port 
--	(
--		SW		: IN 	STD_LOGIC_VECTOR (9 downto 0);
--		HEX0 	: OUT STD_LOGIC_VECTOR (7 downto 0);
--		HEX1 	: OUT STD_LOGIC_VECTOR (7 downto 0)
--	);
--end component;
	
begin

	A(0) <= V(0);
	A(1) <= (not V(3) and V(1)) or ( V(3) and V(2) and not V(1));
	A(2) <= (not V(3) and V(2)) or (V(2) and V(1));
	A(3) <=  ( V(3) and not V(2) and not V(1) );
	
	z <= (V(3) and V(1)) or (V(3) and V(2));
	d0 <= V when z = '0' else A;
	d1 <= "0000" when z = '0' else "0001";
	
	with d0 select
		HEX0 <=
		"11000000" when "0000",	--0
		"11111001" when "0001",	--1
		"10100100" when "0010",	--2
		"10110000" when "0011",	
		"10011001" when "0100",	--4
		"10010010" when "0101",	
		"10000010" when "0110",	
		"11111000" when "0111",	
		"10000000" when "1000",	--8
		"10011000" when "1001",	--9
		"11000000" when "1010",	--0
		"11111001" when "1011",	--1
		"10100100" when "1100",	--2
		"10110000" when "1101",	
		"10011001" when "1110",	--4
		"10010010" when "1111";	--5

	HEX1 <= "11000000" when d1(0) = '0' else "11111001";
	--	U1 : C4M1P1 port map (SW=>SW, HEX0=> );
	
end architecture implementation;